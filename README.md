# Craft

Here is a fork of https://github.com/fogleman/Craft

## Dependencies

	sudo apt install make cmake libglew-dev mesa-common-dev libx11-dev \
	  libxrandr-dev libcurl4-gnutls-dev libxinerama-dev libxcursor-dev \
	  libglfw3-dev check

# Building

You must build in a subdirectory

	mkdir build
	cd build/
	cmake ..
	make
	./craft

# Syncing to original

	git fetch upstream
	git checkout master
	git merge upstream/master

# Keys

	J - Ortho
	F - Fly
	Tab - Menu
	Esc - Deliver mouse

# Addition to the original

* Changed this README.md
* Now build and run in a subdirectory


