#include <GL/glew.h>
#include <GL/glut.h>
#include <GLFW/glfw3.h>
#include <stdio.h>

#include "lodepng.h"
#include "util.h"

GLuint cursor_texture;

void draw_cursor();

void flip_image_vertical(
    unsigned char *data, unsigned int width, unsigned int height)
{
    unsigned int size = width * height * 4;
    unsigned int stride = sizeof(char) * width * 4;
    unsigned char *new_data = malloc(sizeof(unsigned char) * size);
    for (unsigned int i = 0; i < height; i++) {
        unsigned int j = height - i - 1;
        memcpy(new_data + j * stride, data + i * stride, stride);
    }
    memcpy(data, new_data, size);
    free(new_data);
}

void load_png_texture(const char *file_name) {
    unsigned int error;
    unsigned char *data;
    unsigned int width, height;
    error = lodepng_decode32_file(&data, &width, &height, file_name);
    if (error) {
        fprintf(stderr, "load_png_texture %s failed, error %u: %s\n", file_name, error, lodepng_error_text(error));
        exit(1);
    }
    //    flip_image_vertical(data, width, height);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA,
        GL_UNSIGNED_BYTE, data);
    free(data);
}

void
reshape(int w, int h)
{
  /* Because Gil specified "screen coordinates" (presumably with an
     upper-left origin), this short bit of code sets up the coordinate
     system to correspond to actual window coodrinates.  This code
     wouldn't be required if you chose a (more typical in 3D) abstract
     coordinate system. */

  glViewport(0, 0, w, h);       /* Establish viewing area to cover entire window. */
  glMatrixMode(GL_PROJECTION);  /* Start modifying the projection matrix. */
  glLoadIdentity();             /* Reset project matrix. */
  glOrtho(0, w, 0, h, -1, 1);   /* Map abstract coords directly to window coords. */
  glScalef(1, -1, 1);           /* Invert Y axis so increasing Y goes down. */
  glTranslatef(0, -h, 0);       /* Shift origin up to upper-left corner. */
}

void
create_window(int w, int h)
{
  int border = 2;
  glBegin(GL_QUADS);
  glColor4f(0.8, 0.8, 0.8, 0.5f);  /* blue */
  glVertex2i(0, 0);
  glVertex2i(w, 0);
  glVertex2i(w, h);
  glVertex2i(0, h);
  glColor4f(0.2, 0.2, 0.2, 0.5f);  /* blue */
  glVertex2i(2, 2);
  glVertex2i(w-border, border);
  glVertex2i(w-border, h-border);
  glVertex2i(border, h-border);

    glColor4f(1.0, 0.2, 0.2, 0.0f);
  glVertex2i(0.1f, 0.1f);
  glVertex2i(0.1f, 1.0f);
  glVertex2i(1.0f, 1.0f);
  glVertex2i(1.0f, 0.1f);

  glEnd();
}

void
display(void)
{
  glClear(GL_COLOR_BUFFER_BIT);
  //  create_window(600, 400);
  draw_cursor();
  glFlush();  /* Single buffered, so needs a flush. */
}

void
setup_cursor()
{
    glGenTextures(1, &cursor_texture);
    //    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, cursor_texture);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    load_png_texture("../../textures/cursor.png");

}

void draw_cursor()
{
  int h;
  int w = h = 48;
  glDisable(GL_CULL_FACE);
  glDisable(GL_DEPTH_TEST);
  glEnable(GL_BLEND);
  //  glClear(GL_COLOR_BUFFER_BIT);
  glEnable(GL_TEXTURE_2D);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glBindTexture(GL_TEXTURE_2D, cursor_texture);
  glBegin(GL_QUADS);
      glTexCoord2i(0, 0); glVertex2i(100, 100);
      glTexCoord2i(0, 1); glVertex2i(100, 100 + h);
      glTexCoord2i(1, 1); glVertex2i(100+w, 100+h);
      glTexCoord2i(1, 0); glVertex2i(100+w, 100);
  glEnd();
  glDisable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, 0);
  glFlush(); //don't need this with GLUT_DOUBLE and glutSwapBuffers
}

int
main(int argc, char **argv)
{
  glutInit(&argc, argv);
  glutCreateWindow("single triangle");
  glutDisplayFunc(display);
  glutReshapeFunc(reshape);
  setup_cursor();
  glutMainLoop();
  return 0;             /* ANSI C requires main to return int. */
}
