#ifndef _I18N_H_
#define _I18N_H_

#include <libintl.h>
#include <locale.h>

#define _(STRING) gettext(STRING)

#endif // _I18N_H_
