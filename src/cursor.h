/** Mouse cursor handling functions */

#ifndef _CURSOR_H_
#define _CURSOR_H_


/** The mouse mode 
  *
  */
typedef enum
  {
    MM_LOCKED = 0, //! The mouse is in-game and used to mouse player's head
    MM_FREE        //! The mouse is free and can interect with UI
  } mouse_mode_t;

mouse_mode_t get_mouse_mode();
void set_mouse_mode(mouse_mode_t);

void setup_cursor();
void draw_cursor();

#endif // _CURSOR_H_

