#ifndef _FONT_H_
#define _FONT_H_

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#define ALIGN_LEFT 0
#define ALIGN_CENTER 1
#define ALIGN_RIGHT 2


typedef struct {
    GLuint program;
    GLuint position;
    GLuint normal;
    GLuint uv;
    GLuint matrix;
    GLuint sampler;
    GLuint camera;
    GLuint timer;
    GLuint extra1;
    GLuint extra2;
    GLuint extra3;
    GLuint extra4;
} Attrib;

GLuint gen_text_buffer(float x, float y, float n, char *text);
void draw_text(Attrib *attrib, GLuint buffer, int length);
void draw_triangles_2d(Attrib *attrib, GLuint buffer, int count);
void render_text(GLFWwindow*, Attrib*, int, float, float, float n, char *text);

#endif // !_FONT_H_

