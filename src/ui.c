#include "ui.h"

#include <GLFW/glfw3.h>
#include <stdio.h>  // Uses fprintf
#include <stdlib.h> // Uses EXIT_FAILURE
#include <stdarg.h> // Uses va_start/va_end
#include <stddef.h> // Uses sizeof()
#include <string.h> // Uses strcopy()

#include "i18n.h"
#include "list.h"
#include "cursor.h"

/** Defines a to-be-centered dialog 
  *
  * All sizes are in pixels.
  *
  */
typedef struct dialog
{
  int w;  //<! The width of the dialog
  int h;  //<! The height of the dialog
  float r, g, b; //!< The color composants
  float alpha; //!< the alpha channel value (0.0f = transparent, 1.0f=opaque)
  list_t* button_list; // A list of button
}dialog_t;


dialog_t the_dialog;

/** Draws a centered dialog
  *
  * Draws a dialog, centered in the screen using 
  *
  */
void
draw_centered_box(GLFWwindow *win, dialog_t* d)
{
  int width, height;
  glfwGetWindowSize(win, &width, &height);

  int x = (width - d->w) / 2;
  int y = (height - d->h) / 2;

  glBegin(GL_QUADS);
  glColor4f(d->r, d->g, d->b, d->alpha);

  glVertex2i(x, y);
  glVertex2i(x, y + d->h);
  glVertex2i(x + d->w, y + d->h);
  glVertex2i(x + d->w, y);
  
  glVertex2f(0.2f, 0.2f);
  glVertex2f(0.2f, 0.8f);
  glVertex2f(0.8f, 0.8f);
  glVertex2f(0.8f, 0.2f);
  
  glEnd();

}

void
draw_dialog(GLFWwindow *win, int w, int h)
{
  draw_centered_box(win, &the_dialog);

}

button_t*
create_button(int window_width, int button_width, int y, const char* text,
	      button_cb_t cb)
{
  button_t* btn = (button_t*)malloc(sizeof(button_t));
  int x1 = ((window_width - button_width) / 2) + 80;
  btn->x1 = x1;
  btn->y1 = y;
  btn->x2 = x1 + button_width;
  btn->y2 = y+20;
  strcpy(btn->text, text);
  btn->callback=cb;
  return btn;
}

/** The solo button callback
  *
  */ 
void
on_solo_callback()
{
  printf("Solo button clicked!\n");
  set_mouse_mode(MM_LOCKED);
  
}

/** The Multiplayer button callback
  *
  */ 
void
on_multi_callback()
{
  printf("Multi button clicked!\n");
  
}

void
on_options_callback()
{
  printf("Options button clicked!\n");
  
}

void
on_exit_callback()
{
  printf("Exit button clicked!\n");
  
}

void
setup_ui(GLFWwindow * win)
{
  int width, height;
  glfwGetWindowSize(win, &width, &height);

  int border = 2;
  the_dialog.w = 400;
  the_dialog.h = 300;
  the_dialog.r = the_dialog.g = the_dialog.b = 1.0f;
  the_dialog.alpha = 1.0f;

  the_dialog.button_list = list_create();
  int btny = height-310;
  button_t* b1 = create_button(width, 150, btny, _("Solo"), on_solo_callback);
  b1->callback=on_solo_callback;
  list_push(the_dialog.button_list, (void*)b1);

  button_t* b2 = create_button(width, 150, btny-=50, _("Multiplayer"),
			       on_multi_callback);
  list_push(the_dialog.button_list, (void*)b2);

  button_t* b3 = create_button(width, 150, btny-=50, _("Options"),
			       on_options_callback);
  list_push(the_dialog.button_list, (void*)b3);

  button_t* b4 = create_button(width, 150, btny-=80, _("Exit"),
			       on_exit_callback);
  list_push(the_dialog.button_list, (void*)b4);
  
}

/** Render the UI
  *
  * Must be called once per frame, after the world elements.
  *
  */
void
render_ui(GLFWwindow *win, Attrib* text_attrib)
{
  glPushClientAttrib(GL_CLIENT_ALL_ATTRIB_BITS);

  glLoadIdentity();
  glEnable(GL_BLEND);
  glBlendEquation(GL_FUNC_ADD);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glLogicOp(GL_SET);
  glDisable(GL_CULL_FACE);
  glDisable(GL_DEPTH_TEST);
  draw_dialog(win ,600, 400);

  
  LIST_FOR_EACH(the_dialog.button_list, item)
    {
      if (item->val)
	draw_button(win, (button_t*)item->val, text_attrib);
    }
  LIST_FOR_EACH_END(item)
  
  
  glLogicOp(GL_INVERT);
  glDisable(GL_BLEND);
  glEnable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glPopClientAttrib();
}

void draw_button(GLFWwindow *win, button_t* btn, Attrib* text_attrib)
{
  int width, height, size;
  glfwGetWindowSize(win, &width, &height);
  size = 20;
  
  if (btn->intersected)
    size = 25;
  
  render_text(win, text_attrib, ALIGN_CENTER, btn->x1, btn->y2,
	      size, _(btn->text));

}

/** Test if the given x/y (mouse) intercept the given button
  *
  * \return \c true if the mouse intersects the button rectangle shape.
  *
  */
bool
intersect_button(int x, int y, button_t* btn)
{
  btn->intersected = false;

  if (x > btn->x1 && x < btn->x2 && y > btn->y1 && y < btn->y2)
    {
      btn->intersected = true;
    }

  return btn->intersected;
}

/** Inject the mouse into the UI
 * 
 */
void
inject_mouse(int mx, int my)
{

  LIST_FOR_EACH(the_dialog.button_list, item)
    {
      //  draw_button(win, (button_t*)item->val, text_attrib);
      button_t* btn = (button_t*)item->val;
      intersect_button(mx, my, btn);
    }
  LIST_FOR_EACH_END(item)

}

bool
inject_mouse_button(int mouse_button)
{
  if (mouse_button == GLFW_MOUSE_BUTTON_LEFT)
    {
      LIST_FOR_EACH(the_dialog.button_list, item)
	{
	  button_t* btn = (button_t*)item->val;
	  if (btn->intersected)
	    {
	      printf("Handling button '%s'\n", btn->text);
	      btn->callback();
	      break;
	    }
	}
      LIST_FOR_EACH_END(item) 
	}	
}

